package ch.theruins.microspaceempire.test;

import ch.theruins.microspaceempire.test.Cards.SystemCard;

public class Player {
	private int metalStorage = 0, metalProduction, metalStoreMax = 3;
	private int wealthStorage = 0, wealthProduction, wealthStoreMax = 3;
	private int militaryStrength = 0, militaryMax = 3;
	
	private boolean isYearOne = true;
	private boolean isProductionHalted = false;
	
	private static Player instance = null;
	   protected Player() {
	      // Exists only to defeat instantiation.
	   }
	   
	   public static Player getInstance() {
	      if(instance == null) {
	         instance = new Player();
	      }
	      return instance;
	   }
	
	/*
	 * METAL
	 */
	
	public  int getMetalStorage() {
		return metalStorage;
	}
	public  void setMetalStorage(int metalStorage) {
		this.metalStorage = metalStorage;
		if(this.metalStorage >= metalStoreMax){
			this.metalStorage = metalStoreMax;
		}
		System.out.println("MetalStorage: " + this.metalStorage + ", Max: " + this.metalStoreMax);
	}
	public  void incrementMetalStorage(){
		if(metalStorage < metalStoreMax){
			metalStorage++;
		}
	}
	public  void reduceMetalStorage(){
		metalStorage--;
		if(metalStorage <= 0){
			metalStorage = 0;
		}
	}
	public  int getMetalProduction() {
		return metalProduction;
	}
	public  void setMetalProduction(int metalProduction) {
		this.metalProduction = metalProduction;
	}
	
	/*
	 * WEALTH
	 */
	
	public  int getWealthProduction() {
		return wealthProduction;
	}
	public  void setWealthProduction(int wealthProduction) {
		this.wealthProduction = wealthProduction;
	}
	public  int getWealthStorage() {
		return wealthStorage;
	}
	public  void incrementWealthStorage(){
		if(wealthStorage < wealthStoreMax){
			wealthStorage++;
		}
	}
	public  void reduceWealthStorage(){
		wealthStorage--;
		if(wealthStorage <= 0){
			wealthStorage = 0;
		}
	}
	public  void setWealthStorage(int wealthStorage) {
		this.wealthStorage = wealthStorage;
		if(this.wealthStorage >= wealthStoreMax){
			this.wealthStorage = wealthStoreMax;
		}
	}
	
	/*
	 * MILITARY
	 */
	
	public  int getMilitaryStrength() {
		return militaryStrength;
	}
	public  void setMilitaryStrength(int militaryStrength) {
		this.militaryStrength = militaryStrength;
	}
	public  void reduceMilitaryStrength(){
		militaryStrength--;
		if(militaryStrength <= 0){
			militaryStrength = 0;
		}
	}
	public  void incrementMilitaryStrength(){
		if(militaryStrength < militaryMax){
			militaryStrength++;
		}
	}
	public  int getMilitaryMax() {
		return militaryMax;
	}
	public  void setMilitaryMax(int militaryMax) {
		this.militaryMax = militaryMax;
	}

	public  void updateProduction(){
		metalProduction = 0;
		wealthProduction = 0;
		for(int i = 0; i < MainGame.alignedSystemTable.getChildren().size; i++){
			SystemCard card = (SystemCard) MainGame.alignedSystemTable.getChildren().get(i);
			setMetalProduction(metalProduction + card.getMetal());
			setWealthProduction(wealthProduction + card.getWealth());
		}
	}
	
	/*
	 * YEAR MANAGEMENT
	 */
	
	public  void setIsYearOne(boolean isYearOne){
		this.isYearOne = isYearOne;
	}
	public  boolean isYearOne(){
		return isYearOne;
	}
	
	/*
	 * INVASION PENALTY
	 */
	
//	public  void setPenalty(int invasionPenalty){
//		invasionPenalty = invasionPenalty;
//	}
//	public  int getPenalty(){
//		return invasionPenalty;
//	}
	
	/*
	 * INVASION PENALTY
	 */
	
	public  void setIsProductionHalted(boolean isProductionHaltet){
		this.isProductionHalted = isProductionHaltet;
	}
	public  boolean isProductionHalted(){
		return isProductionHalted;
	}
	
	
}
