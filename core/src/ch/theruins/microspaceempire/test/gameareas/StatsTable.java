package ch.theruins.microspaceempire.test.gameareas;

import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.helpers.BuildMilitaryButton;
import ch.theruins.microspaceempire.test.helpers.TextLabel;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class StatsTable extends Group{
	
	private Table table;
	private BuildMilitaryButton buildMilitaryButton;
	
	public StatsTable(){
		table = new Table();
		//table.debug();
		table.setSkin(TextLabel.getLargeSkin());
		//table.setWidth(500);
		table.setHeight(100);
		table.align(Align.left);
		table.setPosition(10, 60);
		table.pad(10);
		table.pack();
		
		updateStats();
		
		addActor(table);
	}

	public void updateStats(){
		table.clear();
		
		buildMilitaryButton = new BuildMilitaryButton("Build", TextLabel.getLargeSkin());
		
		table.add("").pad(10);
		table.add("Metal").padRight(10);
		table.add("Wealth").padRight(10);
		table.add("Military");
		table.row();
		table.add("Prod."); table.add(Integer.toString(Player.getInstance().getMetalProduction())); 
		table.add(Integer.toString(Player.getInstance().getWealthProduction())); 
		table.add(buildMilitaryButton);
		table.row().pad(10);
		table.add("Stock"); table.add(Integer.toString(Player.getInstance().getMetalStorage())); 
		table.add(Integer.toString(Player.getInstance().getWealthStorage())); 
		table.add(Integer.toString(Player.getInstance().getMilitaryStrength()));
	}
	
	public BuildMilitaryButton getBuildMilitaryButton(){
		return buildMilitaryButton;
	}

}
