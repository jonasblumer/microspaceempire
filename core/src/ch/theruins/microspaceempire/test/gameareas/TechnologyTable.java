package ch.theruins.microspaceempire.test.gameareas;

import ch.theruins.microspaceempire.test.helpers.TextLabel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;

public class TechnologyTable extends Table{
	
	public TechnologyTable(){
	//	debug();
		setSkin(TextLabel.getLargeSkin());
		align(Align.left);
		setPosition(600, 20);
		createTable();
		pad(10);
		
		this.pack();

		setBackground(new NinePatchDrawable(getNinePatch(("GUI/btn_default_normal.9.png"))));
		setBackground(new NinePatchDrawable(getNinePatch(("icons/grey.9.png"))));
	}

	public void createTable(){
		align(Align.left);
		add(new TechnologyElement(3, "Capital Ships", "Military Max is 5", 1, 1)).fill();
		add(new TechnologyElement(4, "Forward Starbase", "Explore Distant Systems", 1, 2)).fill();
		row().align(Align.left);
		add(new TechnologyElement(2, "Robot Workers", "Half Production During Strike", 2, 1)).fill();
		add(new TechnologyElement(4, "Planetary Defenses", "+1 Resistance During Invasion", 2, 2)).fill();
		row().align(Align.left);
		add(new TechnologyElement(3, "Hyper Television", "+1 Resistance During Revolt", 3, 1)).fill();
		add(new TechnologyElement(5, "Interstellar Diplomacy", "Next Planet is Aligned", 3, 2)).fill();
		row().align(Align.left);
		add(new TechnologyElement(2, "Interspecies Commerce", "Trade Resources 2 : 1", 4, 1)).fill();
		add(new TechnologyElement(3, "Interstellar Banking", "Storage Max is 5", 4, 2)).fill();

	}
	
	private static NinePatch getNinePatch(String fname) {
	    final Texture t = new Texture(Gdx.files.internal(fname));
	    final int width = t.getWidth() - 2;
	    final int height = t.getHeight() - 2;
	    return new NinePatch(new TextureRegion(t, 1, 1, width, height), 20, 20, 20, 20);
	}


}