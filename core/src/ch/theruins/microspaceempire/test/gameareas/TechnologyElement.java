package ch.theruins.microspaceempire.test.gameareas;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.helpers.TextLabel;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

public class TechnologyElement extends Table{
	
	Skin largeSkin, smallSkin;
	private int price;
	private String title;
	private String subtitle;
	boolean isBought, isBuyable;
	private int tier;
	private int row;
	
	
	Label priceLabel, titleLabel, subtitleLabel;
	
	public TechnologyElement(final int price, String title, String subtitle, final int row, final int tier){
		
		isBought = false;
		isBuyable = false;

		this.tier = tier;
		this.row = row;
		this.align(Align.left);
		
		setTouchable(Touchable.enabled);
		
		this.price = price;
		this.title = title;
		this.subtitle = subtitle;
		
		largeSkin = TextLabel.getLargeSkin();
		smallSkin = TextLabel.getSmallSkin();
		
		this.pad(5);
//		setDebug(true);
		
		priceLabel = new Label("$"+Integer.toString(price), largeSkin); 
		add(priceLabel).left().padRight(5);
		titleLabel = new Label(title, largeSkin); 
		add(titleLabel).left();
		row();
		add();
		subtitleLabel = new Label(subtitle, smallSkin); 
		add(subtitleLabel).left();
		this.setWidth(subtitleLabel.getWidth());
		this.setHeight(subtitleLabel.getHeight()*2);
		//System.out.println(subtitleLabel.getWidth());
		
		
		switch(tier){
		case 1:
			isBuyable = true;
			break;
		case 2:
			isBuyable = false;
			setTextColor(Color.ORANGE);
			break;
		}
		
		addListener( new ClickListener() {              
			
			public void clicked (InputEvent event, float x, float y) {
				if(isBuyable && Player.getInstance().getWealthStorage() >= price){
					Player.getInstance().setWealthStorage(Player.getInstance().getWealthStorage() - price);
					MainGame.statsTable.updateStats();
//					if(tier == 1){
						for (int i = 0; i < MainGame.technologyTable.getChildren().size; i++){
							if(((TechnologyElement) MainGame.technologyTable.getChildren().get(i)).getRow() == row && ((TechnologyElement) MainGame.technologyTable.getChildren().get(i)).getTier() == tier + 1){
								((TechnologyElement) MainGame.technologyTable.getChildren().get(i)).setIsBuyable(true);
							}
						}
//					}
						System.out.println("row: " + row + ", tier: " + tier);
					setTextColor(Color.GREEN);
					isBought = true;
					isBuyable = false;
				}
			}
		});
		
		
		
	}
	
	public void setTextColor(Color color){
		subtitleLabel.setColor(color);
		titleLabel.setColor(color);
		priceLabel.setColor(color);
	}
	
	public int getPrice(){
		return price;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getSubtitle(){
		return subtitle;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getTier(){
		return tier;
	}
	
	public void setIsBuyable(boolean isBuyable){
		this.isBuyable = isBuyable;
		setTextColor(Color.WHITE);
		System.out.println("nxt row: " + row + ", tier: " + tier);
	}
}
