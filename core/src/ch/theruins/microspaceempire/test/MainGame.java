package ch.theruins.microspaceempire.test;

import ch.theruins.microspaceempire.test.Decks.Deck;
import ch.theruins.microspaceempire.test.Decks.DeckBuilder;
import ch.theruins.microspaceempire.test.gameareas.StatsTable;
import ch.theruins.microspaceempire.test.gameareas.SystemTable;
import ch.theruins.microspaceempire.test.gameareas.TechnologyTable;
import ch.theruins.microspaceempire.test.helpers.CombatTable;
import ch.theruins.microspaceempire.test.helpers.EventTable;
import ch.theruins.microspaceempire.test.helpers.GamePhaseHandler;
import ch.theruins.microspaceempire.test.helpers.TextLabel;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class MainGame implements Screen{

	Game game;
	public MainGame(Game game){
		this.game = game;	
	}
	
	
	public static Stage stage;
	public static SystemTable alignedSystemTable, unalignedSystemTable;
	public static CombatTable combatTable;
	public static StatsTable statsTable;
	public static TechnologyTable technologyTable;
	public static EventTable eventTable;
	public static String phase;
	public static TextLabel textLabel;
	public static Deck eventDeck;
	
	
	
	@Override
	public void show() {
		
		phase = "explore";
		
		textLabel = new TextLabel();
		
		Gdx.graphics.setDisplayMode(1136, 640, false);

		// TODO Auto-generated method stub
		stage = new Stage(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		stage.getViewport().apply();
		Gdx.input.setInputProcessor(stage);// Make the stage consume events
		
		alignedSystemTable = new SystemTable();
		alignedSystemTable.setPosition(120, stage.getHeight()-70);
		stage.addActor(alignedSystemTable);
		
		unalignedSystemTable = new SystemTable();
		unalignedSystemTable.setPosition(120, stage.getHeight()-210);
		stage.addActor(unalignedSystemTable);
		
		/*
		zoomTable = new ZoomTable();
		stage.addActor(zoomTable);
		*/
		
		
		statsTable = new StatsTable();
		stage.addActor(statsTable);
		
		technologyTable = new TechnologyTable();
		stage.addActor(technologyTable);
		
		combatTable = new CombatTable();
		stage.addActor(combatTable);
		
		eventTable = new EventTable();
		stage.addActor(eventTable);

		// create near system deck
		Deck nearSystemDeck = DeckBuilder.createDeck(DeckBuilder.deckType.NEAR_SYSTEM, true);
		nearSystemDeck.getCardBackImg().setPosition(10, stage.getHeight()-180);
		stage.addActor(nearSystemDeck.getCardBackImg());
		
		Deck distantSystemDeck = DeckBuilder.createDeck(DeckBuilder.deckType.DISTANT_SYSTEM, true);
		distantSystemDeck.getCardBackImg().setPosition(10, stage.getHeight()-360);
		stage.addActor(distantSystemDeck.getCardBackImg());

		eventDeck = DeckBuilder.createDeck(DeckBuilder.deckType.EVENT, false);
		
		Player.getInstance().updateProduction();
		statsTable.updateStats();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
        
        GamePhaseHandler.handlePhase(phase);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
