package ch.theruins.microspaceempire.test.Decks;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Cards.EventCard;
import ch.theruins.microspaceempire.test.Cards.SystemCard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;


public class DeckBuilder {
	
	public enum deckType {NEAR_SYSTEM, DISTANT_SYSTEM, EVENT};
	
	public static TextureAtlas cardTextures = new TextureAtlas(Gdx.files.internal("cards.txt"));
	
	public static Deck createDeck(deckType type, boolean isSystem){
		
		JsonValue jsonFile = new JsonReader().parse(Gdx.files.internal("data/cards.json"));
		JsonValue jsonObject;
		
		if(isSystem){
			jsonObject = jsonFile.get("System").child;
		}else{
			jsonObject = jsonFile.get("Event");
		}
		
		switch(type){
		case NEAR_SYSTEM:
			return getCards(jsonObject, "NearSystem", isSystem);
		case DISTANT_SYSTEM:
			return getCards(jsonObject, "DistantSystem", isSystem);
		case EVENT:
			return getCards(jsonObject, "Event", isSystem);
		}
		return null;
	}
	
	private static Deck getCards(JsonValue json, String type, boolean isSystem){
		Deck deck = new Deck(type, isSystem);
		if(isSystem){
			for(JsonValue systems = json.child; systems != null; systems = systems.next){
				if(systems.name.toString().contains(type)){
					for(JsonValue system = systems.child; system != null; system = system.next){
						SystemCard card = new SystemCard(system.getString("name"));
						if(isSystem){
							card.setName(system.getString("name"));
							card.setResistance(Integer.parseInt(system.getString("resistance")));
							card.setMetal(Integer.parseInt(system.getString("metal")));
							card.setWealth(Integer.parseInt(system.getString("wealth")));
							card.setVictoryPoints(Integer.parseInt(system.getString("points")));
							if(system.getString("name").contains("HomeWorld")){
								card.scaleDown();
								card.setIsAligned(true);
								MainGame.alignedSystemTable.addActor(card);
							}else{
								deck.getCards().add(card);
							}
						}
					}
				}
			}
		}else{
			for(JsonValue event = json.child; event != null; event = event.next){
				EventCard card = new EventCard(event.getString("name"));
				deck.getCards().add(card);
			}
		}
		deck.getCards().shuffle();
		return deck;
	}
	
	
}
