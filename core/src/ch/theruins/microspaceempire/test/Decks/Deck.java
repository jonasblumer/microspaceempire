package ch.theruins.microspaceempire.test.Decks;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Cards.Card;
import ch.theruins.microspaceempire.test.Cards.SystemCard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

public class Deck extends Actor{
	
	private Array<Card> deck;
	private Texture cardBackTex;
	private Image cardBackImg;
	private String type;
	
	public Deck(final String type, boolean isSystem){
		
		this.type = type;
		
		deck = new Array<Card>();
		
		if(isSystem){
			cardBackTex = new Texture(Gdx.files.internal(type+".png"));
			cardBackImg = new Image(cardBackTex);
			
			cardBackImg.setScale(.3f);
			
			this.setWidth(cardBackImg.getWidth() * getScaleX());
			this.setHeight(cardBackImg.getHeight() * getScaleY());
	
			cardBackImg.addListener( new ClickListener() {              
	            public void clicked(InputEvent event, float x, float y) {
	            	System.out.println("Deck isDone: " + MainGame.combatTable.isDone());
	            	if(MainGame.phase.contains("explore") && !MainGame.combatTable.isVisible() && !MainGame.combatTable.isDone()){
		            	if(deck.size>0){
		            		Card card = deck.pop();
		            		//card.scaleUp();
//		            		card.setWidth(card.getWidth() * card.getScaleX());
//		            		card.setHeight(card.getHeight() * card.getScaleY());
//		            		MainGame.stage.addActor(MainGame.combatTable);
		            		MainGame.combatTable.addCard((SystemCard) card, false);
		            		
		            	}
		            	if(deck.size == 0){
		            		getCardBackImg().setColor(0, 1, 1, 0.8f);
		            	}
	            	}
	            }
	            	
			});
		}
	}
	
	public Array<Card> getCards(){
		return deck;
	}

	public Image getCardBackImg() {
		return cardBackImg;
	}
	
	public String getType(){
		return type;
	}
	
	public Card getCard(){
		return deck.pop();
	}

	
}
