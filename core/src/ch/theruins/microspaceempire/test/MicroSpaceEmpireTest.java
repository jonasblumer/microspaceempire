package ch.theruins.microspaceempire.test;

import com.badlogic.gdx.Game;

public class MicroSpaceEmpireTest extends Game {

	@Override
	public void create () {
		this.setScreen(new MainGame(this));
	}
	
}
