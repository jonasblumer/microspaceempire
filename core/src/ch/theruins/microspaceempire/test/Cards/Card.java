package ch.theruins.microspaceempire.test.Cards;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Decks.DeckBuilder;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class Card extends Group{
	
	public static final float MAX_SCALE = .7f;
	public static final float MIN_SCALE = .2f;
	public static Vector2 MIN_CARD_SIZE; 

	
	private boolean isScaleUp = true;
	
	private Image cardImage;
	
	private String cardName;
	
	public Card(String cardName){
		this.cardName = cardName;
		
		cardImage = new Image(DeckBuilder.cardTextures.findRegion(cardName));
		cardImage.setWidth(DeckBuilder.cardTextures.findRegion(cardName).getRegionWidth());
		cardImage.setHeight(DeckBuilder.cardTextures.findRegion(cardName).getRegionHeight());
		cardImage.setName("Card");
		this.setScale(MAX_SCALE);
		this.setWidth(cardImage.getWidth() * this.getScaleX());
		this.setHeight(cardImage.getHeight() * this.getScaleY());
		MIN_CARD_SIZE = new Vector2(cardImage.getWidth(), cardImage.getHeight());
		
		this.addActor(cardImage);
		
		addListener( new ClickListener() {  
			Vector2 position;
            public void clicked(InputEvent event, float x, float y) {

            }
            /*
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
            	setScale(SystemCard.MAX_SCALE);
            	position = new Vector2(getX(), getY());
            	setPosition(position.x, position.y-300);
            	setZIndex(100);
            }
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor){
            	setScale(SystemCard.MIN_SCALE);
            	setPosition(position.x, position.y);
            	setZIndex(1);
            }
            */
		});
	}
	
	public String getCardName(){
		return cardName;
	}
	
	public void scaleUp(){
		if(!isScaleUp){
			isScaleUp = true;
			this.setScale(MAX_SCALE);
			this.setWidth(cardImage.getWidth()*MAX_SCALE);
			this.setHeight(cardImage.getHeight()*MAX_SCALE);
		}
	}
	
	public void scaleDown(){
		if(isScaleUp){
			isScaleUp = false;
			this.setScale(MIN_SCALE);
			this.setWidth(cardImage.getWidth()*MIN_SCALE);
			this.setHeight(cardImage.getHeight()*MIN_SCALE);
		}
	}
	
	
	
}
