package ch.theruins.microspaceempire.test.Cards;

import ch.theruins.microspaceempire.test.MainGame;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


public class SystemCard extends Card{

	private int resistance, metal, wealth, victoryPoints;
	private StatBubble wealthBubble, resistanceBubble, metalBubble, victoryBubble;
	private boolean isAligned = false;
	
	public SystemCard(String name) {
		super(name);
		addListener( new ClickListener() {              
            public void clicked(InputEvent event, float x, float y) {
            	/*
            	 * fight unaligned systems
            	 */
            	if(!isAligned && !MainGame.combatTable.isDone()){	
            		if(MainGame.phase.contains("explore") && !MainGame.combatTable.isVisible()){
            			//System.out.println("position in Array: " + MainGame.combatTable.);
                		MainGame.combatTable.addCard((SystemCard) event.getTarget().getParent(), false);
            		}	
            	}
            }
		});
	}

	public int getWealth() {
		return wealth;
	}

	public void setWealth(int wealth) {
		this.wealth = wealth;
		wealthBubble = new StatBubble(Integer.toString(wealth), StatBubble.StatType.WEALTH);
		this.addActor(wealthBubble);
	}

	public int getResistance() {
		return resistance;
	}

	public void setResistance(int resistance) {
		this.resistance = resistance;
		resistanceBubble = new StatBubble(Integer.toString(resistance), StatBubble.StatType.RESISTANCE);
		this.addActor(resistanceBubble);
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
		victoryBubble = new StatBubble(Integer.toString(victoryPoints), StatBubble.StatType.VICTORYPOINTS);
		this.addActor(victoryBubble);
	}

	public int getMetal() {
		return metal;
	}

	public void setMetal(int metal) {
		this.metal = metal;
		metalBubble = new StatBubble(Integer.toString(metal), StatBubble.StatType.METAL);
		this.addActor(metalBubble);
	}
	
	public void scaleUp(){
		super.scaleUp();
		metalBubble.setVisible(false);
		wealthBubble.setVisible(false);
		resistanceBubble.setVisible(false);
		victoryBubble.setVisible(false);
	}
	
	public void scaleDown(){
		super.scaleDown();
		metalBubble.setVisible(true);
		wealthBubble.setVisible(true);
		resistanceBubble.setVisible(true);
		victoryBubble.setVisible(true);
	}
	
	public void setIsAligned(boolean isAligned){
		this.isAligned = isAligned;
	}

}
