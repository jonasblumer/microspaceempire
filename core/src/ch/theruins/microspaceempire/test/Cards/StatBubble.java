package ch.theruins.microspaceempire.test.Cards;

import ch.theruins.microspaceempire.test.helpers.TextLabel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class StatBubble extends Group{
	private Label statText;
	private Texture tex;
	private Image image; 
	private TextLabel widget;
	
	public static enum StatType {
		METAL, WEALTH, RESISTANCE, VICTORYPOINTS
	};
	
	public StatBubble(String text, StatType type){
		widget = new TextLabel();
		statText = widget.createTextField(text);
		statText.setFontScale(5);
		statText.setColor(Color.DARK_GRAY);
		
		this.setTouchable(Touchable.disabled);
		
		switch(type){
		case METAL:
			tex = new Texture(Gdx.files.internal("icons/metal.png"), true);
			image = new Image(tex);
			this.setWidth(image.getWidth()*image.getScaleX());
			this.setHeight(image.getHeight()*image.getScaleY());
			this.setPosition(0, Card.MIN_CARD_SIZE.y-this.getHeight()*this.getScaleY());
			break;
		case WEALTH:
			tex = new Texture(Gdx.files.internal("icons/wealth.png"), true);
			image = new Image(tex);
			this.setWidth(image.getWidth()*image.getScaleX());
			this.setHeight(image.getHeight()*image.getScaleY());
			this.setPosition(Card.MIN_CARD_SIZE.x-this.getWidth()-this.getScaleX(), Card.MIN_CARD_SIZE.y-this.getHeight()*this.getScaleY());
			break;
		case RESISTANCE:
			tex = new Texture(Gdx.files.internal("icons/resistance.png"), true);
			image = new Image(tex);
			this.setWidth(image.getWidth()*image.getScaleX());
			this.setHeight(image.getHeight()*image.getScaleY());
			break;
		case VICTORYPOINTS:
			tex = new Texture(Gdx.files.internal("icons/victorypoint.png"), true);
			image = new Image(tex);
			this.setWidth(image.getWidth()*image.getScaleX());
			this.setHeight(image.getHeight()*image.getScaleY());
			this.setPosition(Card.MIN_CARD_SIZE.x-this.getWidth()-this.getScaleX(), 0);
			break;
		default:
			tex = new Texture(Gdx.files.internal("icons/resistance.png"), true);
		}
		
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		statText.setPosition((image.getWidth()*image.getScaleX())/2 - (statText.getWidth()*statText.getFontScaleX())/2, (image.getHeight()*image.getScaleX())/2-(statText.getHeight())/2);
	
		this.addActor(image);
		this.addActor(statText);
	}

}
