package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.Cards.Card;
import ch.theruins.microspaceempire.test.Cards.SystemCard;

public class EventHandler {
	
	public static void handleEvent(String eventName){
		
		switch(eventName){
			case"Asteroid":
				Player.getInstance().incrementWealthStorage();
				break;
			case "DerelictShip":
				Player.getInstance().incrementMetalStorage();
				break;
			case "LargeInvasion":
				/*
				 * against last empire
				 */
				if(Player.getInstance().isYearOne()){
					MainGame.combatTable.setPenalty(2);
					handleInvasion();
				}else{
					MainGame.combatTable.setPenalty(3);
					handleInvasion();			}
				break;
			case "SmallInvasion":
				/*
				 * against last empire
				 */
				if(Player.getInstance().isYearOne()){
					MainGame.combatTable.setPenalty(1);
					handleInvasion();
				}else{
					MainGame.combatTable.setPenalty(2);
					handleInvasion();
				}
				break;
			case "Revolt1":
				/*
				 * against least resistance
				 */
				if(Player.getInstance().isYearOne()){
					MainGame.combatTable.setIsDone(false);
					MainGame.combatTable.setPenalty(1);
					handleRevolt();
				}else{
					MainGame.combatTable.setIsDone(false);
					MainGame.combatTable.setPenalty(2);
					handleRevolt();
				}
				break;
			case "Revolt2":
				/*
				 * against least resistance
				 */
				if(Player.getInstance().isYearOne()){
					MainGame.combatTable.setIsDone(false);
					MainGame.combatTable.setPenalty(1);
					handleRevolt();
				}else{
					MainGame.combatTable.setIsDone(false);
					MainGame.combatTable.setPenalty(3);
					handleRevolt();
				}
				break;
			case "Strike":
				Player.getInstance().setIsProductionHalted(true);
				break;
				default:
					System.out.println("nothin");
					break;
		}
	}
	
	private static void handleInvasion(){
		SystemCard card = (SystemCard) MainGame.alignedSystemTable.getChildren().get(MainGame.alignedSystemTable.getChildren().size-1);
		if(!card.getCardName().contains("HomeWorld")){
			MainGame.combatTable.addCard(card, true);
		}
	}
	
	private static void handleRevolt(){
		int leastResistance = 100;
		SystemCard leastResistanceCard = null;
		int resistance;
		for(int i = 0; i < MainGame.alignedSystemTable.getChildren().size; i++){
			//resistance =
			SystemCard card = (SystemCard) MainGame.alignedSystemTable.getChildren().get(i);
			resistance = card.getResistance();
			System.out.println(card.getCardName() + ": res: " + resistance);
			if(resistance <= leastResistance && !card.getCardName().contains("HomeWorld")){
				leastResistance = resistance;
				leastResistanceCard = card;
				System.out.println("leastRes: " + leastResistance);
			}
		}
		if(leastResistanceCard != null){
			MainGame.combatTable.addCard(leastResistanceCard, true);
		}
	}
}
