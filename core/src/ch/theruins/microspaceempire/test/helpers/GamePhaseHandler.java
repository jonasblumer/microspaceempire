package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.Cards.EventCard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class GamePhaseHandler {
	
	public static void handlePhase(String phase){
		System.out.println("phase: " + phase);
		switch(phase){
        case "explore":
        	if(Gdx.input.isKeyPressed(Keys.SPACE)){
        		if(MainGame.combatTable.isDone()){
        			System.out.println("space in explore");
	        		if(MainGame.combatTable.followsEvent()){
	        			MainGame.phase = "explore";
	        			MainGame.combatTable.setIsDone(false);
	        		}else{
	        			MainGame.phase = "collect";
	        		}
        		}
        	}
        	break;
        case "collect":
        	MainGame.combatTable.setIsDone(false);
        	//combatTable.setIsDone(false);
//        	System.out.println("COLLECTION PHASE");
        	if(!Player.getInstance().isProductionHalted()){
        		Player.getInstance().setMetalStorage(Player.getInstance().getMetalStorage() + Player.getInstance().getMetalProduction());
        		Player.getInstance().setWealthStorage(Player.getInstance().getWealthStorage() + Player.getInstance().getWealthProduction());
        		MainGame.statsTable.updateStats();
        	}else{
        		Player.getInstance().setIsProductionHalted(false);
        	}
        	
//        	System.out.println("(TRADE SUBPHASE)");
        	//if(Gdx.input.isKeyPressed(Keys.SPACE)){
        		MainGame.phase = "build";
        	//}
        		MainGame.statsTable.getBuildMilitaryButton().setIsIncreased(false);
        	break;
        case "build":
        	System.out.println("BUILD PHASE");
        	//if(Gdx.input.isKeyPressed(Keys.SPACE)){
        		MainGame.phase = "event";
        	//}
        	break;
        case "event":
        	/*
        	 * EventCard eC = eventDeck.getCard().getCardName()
        	 * EVENT TABLE ADD(eC)
        	 * EVENT HANDLER(HANLDE(eC))
        	 */
        	if(MainGame.eventDeck.getCards().size>0){
        		if(MainGame.eventTable.getTable().getChildren().size < 1){
            		System.out.println("EVENT PHASE");
        			EventCard card = (EventCard) MainGame.eventDeck.getCard();
        			MainGame.eventTable.addCard(card);
        		}
        	}else{
    			if(Player.getInstance().isYearOne()){ 
    				Player.getInstance().setIsYearOne(false);
    				/*
    				 * FILL EVENT DECK AGAIN!!!
    				 */
    			}else{
    				System.out.println("GAME OVER!!!!");
    			}
    		}
        	//if(Gdx.input.isKeyPressed(Keys.SPACE)){
        		MainGame.phase = "explore";
        	//}
        	break;
        }
	}
}
