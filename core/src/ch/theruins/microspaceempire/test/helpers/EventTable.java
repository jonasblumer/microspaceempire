package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.Cards.EventCard;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

public class EventTable extends Group{
	
	private Table table;
	private EventCard card;
		
	public EventTable(){
		
		this.setVisible(false);
		setWidth(MainGame.stage.getWidth());
		setHeight(MainGame.stage.getHeight());
		
		/*
		 * table for combat info
		 */
		table = new Table();
		table.debug();
		table.setWidth(500);
		table.setHeight(500);
		table.align(Align.center);
		

		addActor(table);
		
		/*
		 * set background color
		 */
		Pixmap pm1 = new Pixmap(1, 1, Format.RGB565);
		pm1.setColor(Color.NAVY);
		pm1.fill();
		table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
		
		addListener( new ClickListener() {              
        
			public void clicked(InputEvent event, float x, float y) {
				EventHandler.handleEvent(card.getCardName());
    			MainGame.statsTable.updateStats();
				setVisible(false);
				table.clear();
				MainGame.statsTable.updateStats();

			}
		});
	}
	
	public void addCard(EventCard card){
		card.scaleUp();
		this.setVisible(true);
		this.card = card;
		table.add(card).left();
		table.pack();
		table.setPosition(MainGame.stage.getWidth()/2 - table.getWidth()/2, MainGame.stage.getHeight()/2 - table.getHeight()/2);
	}
	
	public Table getTable(){
		return table;
	}
}
