package ch.theruins.microspaceempire.test.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class TextLabel{
	
	private static Skin largeTextSkin;
	private static Skin smallTextSkin;
	
	public TextLabel(){
		
		/*
		 * large font
		 */
		largeTextSkin = new Skin();
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Medium.ttf"));
	  	FreeTypeFontParameter parameter = new FreeTypeFontParameter();
	  	parameter.size = 20;
	  	BitmapFont font = generator.generateFont(parameter); // font size 12 pixels
	  	largeTextSkin.add("default", font);
	  	Label.LabelStyle textFieldStyle = new Label.LabelStyle();
	  	textFieldStyle.font = largeTextSkin.getFont("default");
	  	largeTextSkin.add("default", textFieldStyle);
	  
	  	/*
	  	 * small font
	  	 */
	  	smallTextSkin = new Skin();
	  	FreeTypeFontGenerator generator2 = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Medium.ttf"));
	  	FreeTypeFontParameter parameter2 = new FreeTypeFontParameter();
	  	parameter2.size = 14;
	  	BitmapFont font2 = generator2.generateFont(parameter2); // font size 12 pixels
	  	smallTextSkin.add("default", font2);
	  	Label.LabelStyle textFieldStyle2 = new Label.LabelStyle();
	  	textFieldStyle2.font = smallTextSkin.getFont("default");
	  	smallTextSkin.add("default", textFieldStyle2);
	}

	public Label createTextField(String text){
		Label txt = new Label(text, largeTextSkin);
		return txt;
	}
	
	public static Skin getLargeSkin(){
		return largeTextSkin;
	}
	public static Skin getSmallSkin(){
		return smallTextSkin;
	}
}
