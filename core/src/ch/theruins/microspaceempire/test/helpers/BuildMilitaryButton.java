package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class BuildMilitaryButton extends Label{
	
	private static boolean isIncreased = false;
	
	public BuildMilitaryButton(CharSequence text, Skin skin) {
		super(text, skin);
		// TODO Auto-generated constructor stub
		this.setColor(Color.GREEN);
		
		addListener( new ClickListener() {              
			public void clicked (InputEvent event, float x, float y) {
				if(Player.getInstance().getMetalStorage() > 0 && Player.getInstance().getWealthStorage() > 0 && Player.getInstance().getMilitaryStrength() < Player.getInstance().getMilitaryMax()){
					if(!isIncreased /*&& MainGame.phase.contains("build")*/){
						isIncreased = true;
						System.out.println(isIncreased);
						Player.getInstance().incrementMilitaryStrength();
						Player.getInstance().reduceMetalStorage();
						Player.getInstance().reduceWealthStorage();
						MainGame.statsTable.updateStats();
					}
				}
			}
		});
		
	}
	
	public void setIsIncreased(boolean isIncreased){
		BuildMilitaryButton.isIncreased = isIncreased;
	}
	
}
