package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.Cards.SystemCard;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

public class CombatTable extends Group{
	
//	private Actor clickBlocker;
	private Table table;
	private SystemCard card;
	
	private int penalty = 0;
	
	private int rand;
	
	private boolean followsEvent = false;
	
	private boolean isDone = false;
	
	public CombatTable(){
		
		this.setVisible(false);
		setWidth(MainGame.stage.getWidth());
		setHeight(MainGame.stage.getHeight());
		
		/*
		 * table for combat info
		 */
		table = new Table();
		table.debug();
//		table.setWidth(800);
//		table.setHeight(500);
		table.align(Align.left);
		table.setZIndex(500);
		
		/*
		 * set background color
		 */
		Pixmap pm1 = new Pixmap(1, 1, Format.RGB565);
		pm1.setColor(Color.BLUE);
		pm1.fill();
		table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
		
		addActor(table);
		
		addListener( new ClickListener() {              
        
			public void clicked(InputEvent event, float x, float y) {
				if(!isDone){
					doCombat();
					isDone = true;
				}
			}
		});
		rand = MathUtils.random(5)+1;
	}
	
	public void addCard(SystemCard card, boolean followsEvent){
		isDone = false;
		this.followsEvent = followsEvent;
		card.scaleUp();
		card.setZIndex(999);
		this.setVisible(true);
		this.setScale(0);
		this.setOrigin(getWidth()/2, getHeight()/2);
		this.addAction(Actions.scaleTo(1, 1, .3f));
		this.card = card;
		
		rand = MathUtils.random(5)+1;
		table.add(card).left();
		table.add(MainGame.textLabel.createTextField(card.getResistance() + " + penalty (" + penalty + ") >= RAND ("+rand+") + " + Player.getInstance().getMilitaryStrength())).right();
		table.pack();
		table.setPosition(MainGame.stage.getWidth()/2 - table.getWidth()/2, MainGame.stage.getHeight()/2 - table.getHeight()/2);
	}
	
	private void doCombat(){
		if(rand + Player.getInstance().getMilitaryStrength() >= card.getResistance() + penalty){
			card.setIsAligned(true);
			MainGame.alignedSystemTable.addActor(card);
		}else{
			Player.getInstance().reduceMilitaryStrength();
			card.setIsAligned(false);
			MainGame.unalignedSystemTable.addActor(card);
		}
		card.scaleDown();
		Player.getInstance().updateProduction();
		this.setVisible(false);
//		if(followsEvent){
//			MainGame.phase = "explore";
//		}else{
//			MainGame.phase = "collect";
//		}
		table.clear();
		penalty = 0;
		isDone = true;
	}
	
	public Table getTable(){
		return table;
	}
	
	public void setPenalty(int penalty){
		this.penalty = penalty;
	}
	
	public boolean followsEvent(){
		return followsEvent;
	}
	
	public boolean isDone(){
		return isDone;
	}
	
	public void setIsDone(boolean isDone){
		this.isDone = isDone;
	}
}
