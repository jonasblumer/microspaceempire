package ch.theruins.microspaceempire.test.helpers;

import ch.theruins.microspaceempire.test.MainGame;
import ch.theruins.microspaceempire.test.Player;
import ch.theruins.microspaceempire.test.Cards.Card;
import ch.theruins.microspaceempire.test.Cards.SystemCard;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

public class ZoomTable extends Group{
	
	protected Table table;
	
	public ZoomTable(){
		setWidth(MainGame.stage.getWidth());
		setHeight(MainGame.stage.getHeight());
		
		/*
		 * table for combat info
		 */
		table = new Table();
		table.debug();
		table.align(Align.left);
		table.setPosition(MainGame.stage.getWidth()/2 - table.getWidth()/2, MainGame.stage.getHeight()/2 - table.getHeight()/2);

		addActor(table);
		
		addListener( new ClickListener() {              
        
			public void clicked(InputEvent event, float x, float y) {
			}
		});
	}
	
	public void addCard(SystemCard card){
		card.scaleUp();
		this.setVisible(true);
		
		table.add(card).left();
		
	}
}
